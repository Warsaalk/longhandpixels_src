---
title: What is Responsive Web Design?
pub_date: 2014-02-12 12:04:25
slug: /blog/2014/02/what-is-responsive-web-design
metadesc: A gentle introduction to responsive web design... hint, it's more than flexible sites with some media queries
tags: Responsive Web Design

---

[*Note: This is an excerpt from my book, [Build a Better Web With Responsive Web Design](https://longhandpixels.net/books/responsive-web-design). This starts out pretty simple, but even if you're already familiar with the basic concept of responsive design you might learn a few new things. My definition of responsive web design is very braod, encompassing ideas like taking a mobile-first approach and using progressive enhancement to make sure you responsive web site works everywhere.*]

The phrase responsive design was coined by Ethan Marcotte in 2009 in an *A List Apart* article, entitled, appropriately enough, *[Responsive Web Design](http://www.alistapart.com/articles/responsive-web-design/)*. At the most basic level Marcotte used the phrase to mean building websites that respond to users' needs.

The specific case Marcotte wrote about was making websites flow into different layouts for a variety of screens, especially mobile. When Marcotte's article first appeared in 2009 the iPhone was just starting to be truly ubiquitous. Clients were asking for "iPhone websites". But it wasn't just the iPhone, the handheld device market was just about to explode. There was a fear that the web would devolve into a multitude of separate websites, each tailored to a specific device, which, as Marcotte and others recognized, would be insane.

As Marcotte writes "can we really continue to commit to supporting each new user agent with its own bespoke experience? At some point, this starts to feel like a zero sum game. But how can we -- and our designs -- adapt?"

The answer was two-fold. First there were some new tools available to help us out, namely the @media query in CSS 3. Then there was the second and more powerful part, which meant going back to the web's origins and rediscovering something many of us lost along the way -- the web is an inherently flexible medium.

In practice responsive design means creating websites that look good no matter which screen they might be on. Building a responsive website means making sure that the site is easy to read and navigate with a minimum of resizing, scrolling or panning. Building a responsive website means building a site your users and customers will recognize and enjoy regardless of which device they might be using -- mobile phone, tablet, laptop, desktop or even the internet enabled toaster of the future.

![Marcotte's original responsive demo site, shown here at roughly phone, tablet and desktop sizes](images/marcotte-demo.png)

It sounds wonderful at first blush -- who doesn't want their website to look great and work well on any screen? Even those we don't know about yet? Stop and think about it for a bit though and suddenly responsive design starts to sound unfathomably complex. 

How in the world do you make your site look good no matter where it's being served up -- phone, tablet, ebook reader, laptop, desktop and more?

To complicate matters even those nice clean divisions -- mobile, tablet and desktop -- are fast disappearing. There are phones with tablet-like 7-inch HD screens, desktop-size monitors that run Android 4.0 and hybrid devices like Ubuntu's mobile OS which can dock to a monitor -- is that mobile? Is it a desktop? What if the answer to both questions is yes?

Also consider that the "screens" of the future might not be screens at all. Google Glass is already in the wild and while it's still a "screen" of sorts, it's certainly different from what most of us are used to. Several years ago Microsoft showed off a prototype projection device dubbed the "OmniTouch" which essentially put the "screen" anywhere -- the desk in front of you, the wall during a presentation, your hand, anywhere. Other, somewhat more likely to actually make it to market "screens" include "smart" glass for windows, that can can pull all sort of tricks, including turning opaque to become a display. At the other end of the spectrum Sony is hard at work on displays that behave like and are no thicker than a single sheet of paper.

![Google glasses. Say what you will, they're out there and they want to load your site. <small>Image credit: Giuseppe Costantino, [CC/Flickr](http://www.flickr.com/photos/69730904@N03/8813574238/).</small>](images/google-glass.png)

Some of these ideas will become part of our reality, some will not. Other ideas we can't even imagine right now will also be created. 

The "screen" of the future might be your sunglasses, your hand, the back of the seat in front of you on the bus or the window by your bed when you wake up in the morning. Which of these is mobile, which is a tablet and which is a desktop? To build a future-proof web we need to stop focusing so heavily on individual devices, yes, but why stop there? While we're at it we might as well get rid of the notion of device categories as well since the hardware has already started doing exactly that. In the end even thinking of "screens" will soon seem antiquated. For this reason most web standards rarely use the word "screen", opting instead to talk about "viewports". 

Ultimately it makes no sense to frame our discussions by device type or even context. All of these devices, these screens, these viewports, these *things* are just portals into the world of the web. The more portals that can see into the web the more people you can bring into your world. 

The future of the web is a chaos and confusion of portals. Building a tricked out site for each of them is an insane idea today and it will be even more insane two years, five years, twenty years from now.

So what do we do? Well, we could wait and see. Keep building sites that target individual devices until we collapse under the weight of the work (or price ourselves out of anything clients would be willing to pay). Or we can take what developers like Brad Frost call a "future-friendly" approach. That is, we can do the best we can with the tools we have available today and make decisions on which tools to use based on which are the most likely to work in the future. 

Let's step back in time for a minute to 2007 and consider the developer's dilemma when the iPhone first launched. You need to embed a movie in your page. The do-nothing approach would dictate you just embed a Flash player and call it a day. No Flash? Too bad. Apple will come around because, well, everyone has Flash. Except that we all know how that turned out.

If instead we took a more future-friendly approach we might embed the movie using HTML5's video tag, while offering a Flash fallback for browsers that don't support modern web standards. This would have meant a bit more work at the time since there's a bit more code to write and we would have had to figure out exactly how to do it. 

But the easier, "do-nothing" approach would have meant more work down the road when you had to convert your site to HTML5 video anyway since even Adobe has abandoned the idea of including Flash on mobile devices.

What can we learn from this little example? Well, first and foremost we need to embrace solutions that work today. There's no point in designing *only* for the future. In this case going with only HTML5 video tags would probably have been a bad choice; in 2007 you still needed a Flash-based fallback. 

We need to make our sites work well with the web as it is, but we should also keep an ear cocked toward the future and embrace those tools and design patterns that are most likely to work with the devices of the future as well. Brad Frost put it quite well when he said, "We don't know what will be under Christmas trees two years from now, but that's what we need to design for today."

Right now that means using the responsive design tools and best practices that follow to build websites. 

Let's start with the three basic tools of responsive design -- fluid layouts, media queries and flexible media. 

*   *Fluid Layouts*: By defining our content grids in mathematical proportions rather than pixels our content will fit any screen. So instead of having a 750px main column and a 250px sidebar, the columns would be defined as 75% and 25% respectively.
*    *Media Queries*: Media queries are a CSS feature that allow styles to be applied conditionally, based on criteria such as screen width and pixel density. With as little as 3 or 4 lines of code you can resize your entire website and re-flow content to fit different screen sizes.
*    *Flexible Media*: Dimensions of images, video, and animations should be flexible and adapt to suit different screen sizes similar to how grids should be fluid.
            
To these three core principles of responsive design I am adding two more -- mobile-first design and progressive enhancement.

*   *Mobile-first Design*: Start by making sure your site and its content work on the least capable devices your visitors are using. By all means build as fancy and JavaScripty of a site as you want; just do it on basic, solid foundations.
*   *Progressive Enhancement*: Don't *stop* with that most basic version of your site; start there. Then layer in complexity and more advanced features for more capable devices, progressively enhancing it as the devices become more capable.

That may sound like a lot of stuff to keep track of, but guess how many of the things in this list are actually new? 

Just one, @media queries. Everything else is almost as old as the web. That means you don't really have to learn anything new, you just need to shift your approach in some subtle, but profound ways.

Implementing responsive design is simple, but, as they say, the devil is in the details.

That's where this book comes in. You bought this book, which means you're open to new ideas and new workflows. That's good because I'm going to challenge some long-held assumptions behind many of the sites you've probably built. I'm also going to tell you that, most likely, you've been doing it wrong, as they say. That's okay, I did it wrong for years too. Sure the sites we built worked, after all the biggest challenge we had was making things work in IE 6. And work they did, but we were still working from flawed premises and it's time to change that.

It's time to step back from our toolkits and workflows and question everything because those fixed width sites designed for desktop screens don't work well on smartphones and probably don't work at all on feature phones. They probably won't look all that great in Google Glass or projected directly onto your retina either. As William Gibson said, the future is already here, it's just unevenly distributed. And that's what we need to develop, websites and web apps capable of handling the uneven distribution of the future.
