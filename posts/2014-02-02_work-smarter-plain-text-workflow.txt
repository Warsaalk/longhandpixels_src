---
title: "Work Smarter: The Plain Text Workflow"
pub_date: 2014-02-02 19:37:07
slug: /blog/2014/02/work-smarter-plain-text-workflow
metadesc: A guide to smarter responsive design workflows. If you're still designing websites in Photoshop you're doing too much work. 
tags: Responsive Web Design, Building Smarter Workflows
code: True
tutorial: True
---

*If you've ever struggled building responsive websites, this post is for you. It's part of a series on responsive design and smarter workflows, all pulled from my book, [Responsive Web Design](https://longhandpixels.net/books/responsive-web-design). If you find this excerpt useful, and want even more ideas on how responsive design can help you create amazing websites, sign up for the newsletter below and you'll get a discount when the book is released.*

[TOC]

If your current workflow looks anything like mine used to, you probably start most of your design work in a graphics editor like Photoshop. There's nothing "wrong" with that per say, nor is Photoshop some tool for evil, but I'm here to tell you there is a better, more productive way to work.

Here's the thing: if you're designing websites in Photoshop you're doing too much work. 

If you're building responsive websites and you're not working directly with the code in a web browser you're wasting time and energy using the wrong tool for the job. There's nothing wrong with Photoshop, but it's not the best tool for developing responsive websites.

Why?

Photoshop has a fixed canvas size; the web has an infinitely flexible canvas. Responsive design means embracing that flexible canvas and you can't do that when you're working in a graphics editor.

The web browser is the only tool I know of that offers the same fluid, flexible canvas of the web. **If you want to simplify and optimize your responsive workflow, the place to work is in the browser**. 

Designing in the browser means working with the web rather than against it, and that's the first step toward a simpler, faster responsive design workflow.

##Faster, Easier Web Development? Sign Me Up!

For a long time I ignored the advice to design in the browser because often the people giving it stopped with the advice. Go design in the browser! It's awesome! And, unicorns! Whereas I would just sit there thinking, *Uuuuuuuh, okay, but what the heck does that mean? Design in the browser...? Grumble, well, I've got work to do and I know how to use Photoshop...*

Designing directly in the web browser makes sense at a theoretical level, but what does it mean in practical terms? What does this workflow look like and where do you start?

That's why I wrote this, to outline how I finally figured out I could save tremendous time and effort working directly in the browser rather than prototyping everything in Photoshop. I'm going to share my workflow in hopes it will prove useful to you. 

Before we dive in, realize that this is just my workflow. It's not THE workflow by any means. Whether you're part of a small team, large team or working on your own, you have your idiosyncrasies and tics to consider. A good workflow will work with, not against you. I can't tell you how you should work. I can, however, tell you how I work and hopefully that will give you some ideas you can test in your own workflow. Take what you need, skip what you don't.

In the [first part of this responsive design workflow series](https://longhandpixels.net/blog/2013/11/easiest-way-get-started-designing-browser) we looked at a super simple way to serve files on our local development machines. After all, you can't design in the browser if you don't have a server for your web browser to talk to. So I'll assume you've completed that tutorial (or have your own web server setup already). 

## Everything Starts with the Content

The first thing to do with any tough question is to break it down into smaller questions. So instead of asking what does designing in the browser look like, let's start with a more basic question: What do we need before we can display a webpage?

The simple answer is: the content.

Before we can do anything with HTML or CSS we need the contents of our site. Everything starts with content. No content, no HTML. No HTML, no CSS. 

The foundation of any design has to be the content. I know what you're thinking; you're thinking I'll just grab some Lorem Ipsum. Or, if you prefer some snarky for your dev work, [Samuel L. Ipsum](http://slipsum.com). But that's not going to cut it. 

Without the content we're designing in the dark. I can't remember where I first saw a graphic like this, but it's what turned the light bulb on for me.... Simply put, there's no point in designing a layout with this:

[![two plain lorem ipsum paragraphs](https://longhandpixels.net/media/images/2014/contentfirst1.png)](https://longhandpixels.net/media/images/2014/contentfirst1.png "View Image 1")
: The content you think you're going to get.

When what you're actually need to display looks like this:

[![structurally complex content with lists, paragraphs and blockquotes](https://longhandpixels.net/media/images/2014/contentfirst2.png)](https://longhandpixels.net/media/images/2014/contentfirst1.png "View Image 2")
: The content you actually got.


As Jeffery Zeldman [writes](http://www.zeldman.com/2008/05/06/content-precedes-design/), "design in the absence of content is not design, it's decoration." We're not decorating websites, we're designing them, which means we need actual content.

In terms of workflow, this means you need to get the content as soon as you can. No content, no work to flow. It means that when you're meeting with clients you need to emphasize the importance of having the actual content, not placeholders, but actual content. Without the content you can't make the client's vision a reality. You need real content to build real websites.

If you're doing a redesign of existing content, you're all set, just dump the site using a tool like [curl](http://curl.haxx.se) or wget[^1] and then do a content inventory -- list all the contents and make a special note of any patterns, repeated content and outlier content you find.  

Once you have the content you can move on to sketching and wireframing -- figuring out the structure of the pages and overall site. I tend to do this stage mostly on paper because I find that to be the fastest way to get my ideas recorded, but I've seen other developers who use iPads with styluses, mind mapping tools, even one who used spreadsheets to create wireframes (yeah, he was a little strange). 

Once I have a good idea of the hierarchy and structure of the content on the site, I immediately jump into the browser and start working with live HTML. At this point I'm just stacking text and creating structure, there's no CSS yet.

There are two necessary components in this workflow: the web server and the files.

##Tool #1: The Web Server

For the full details on how you can start a web server in any folder on your computer, see the first part of this series. To quickly recap where we left off, recall that we set up our example project in `~/Sites/myproject/`. We then open up our terminal application and type:

~~~language-bash
cd ~/Sites/myproject/ && serve
~~~

Assuming you set up the aliases outlined in the first post, this will start up a server in the `myproject` folder. Now point your browser to [http://localhost/:8080](http://localhost/:8080) and you should see a directory listing of... nothing! That's okay, the important thing is that we've got a server up and running. 

Now we're ready to turn our content into actual HTML files.

##Tool #2: Plain Text

Up until now I've been using a phrase I don't particularly like -- "the content". That makes it sound like it's just a pile of stuff. But it's not. It's words, phrases, sentences, pitches, headlines, sub headlines, outlines, lists, tables, buttons, forms, charts, illustrations, images, videos. "The content" is generic, the contents of the site you're building are not. This isn't just semantics, it's your first clue in how to get your workflow in line with the web itself.

Notice two that almost everything in that list is either text, an image or a video. 

At its core this is what the web is made of -- text and images. This is why starting your work in a graphics editor doesn't make sense. The web is mostly text. Even fancy landing pages and ultra-slick web apps are ultimately about serving up text and images in the some way.

And within that core, for most sites, "the content" is text. So when we're designing in the browser we'll start where the web does, with text. Then we'll add structure to that text. Then we can actually **design** pages optimized to display that structured text rather than just decorating some filler and hoping for the best.

### The Power of Markdown

Starting with just the text makes it much easier to see the structure of your content and mark it up accordingly. 

To my mind the best way to markup your documents at this stage is to use John Gruber's [Markdown](http://daringfireball.net/projects/markdown/). Markdown is a text-to-HTML conversion tool which allows you to markup text using an easy-to-read, easy-to-write format and then convert it to structurally valid HTML. While it won't allow you to markup every thing you might need in the end, it's perfect for generating quick prototypes like this. You can read through the [Markdown documentation over on Daring Fireball](http://daringfireball.net/projects/markdown/syntax).

Best of all the syntax of Markdown is extremely simple, you can pick it up in an afternoon and reach the level of mastery we need in a day or two. 

To markup our content we'd just dive into our plain text files and add a bit of structure. Let's say you had some text that looks like this:

~~~{language-markup}
Acme Widgets

We're Awesome Widget Makers

Crank your widgets faster with ACME Widgets

Our Widgets are the best in the business. You can rely on ACME widgets day in and day out. The toughest, most dependable widgets out there.

What can ACME Widgets do for you?

We can help you build better subwidgets
We can make you life widgetier
We can solve all your widget headaches
~~~

Let's use Markdown to add some structure. The result might look something like this:

~~~language-markup
#We're Awesome Widget Makers

##Crank your widgets faster with ACME Widgets

Our Widgets are the best in the business. You can rely on ACME widgets day in and day out. The toughest, most dependable widgets out there.

###What can ACME Widgets do for you?

* We can help you build better subwidgets
* We can make your life widgetier
* We can solve all your widget headaches
~~~

You can refer to the [Markdown docs](http://daringfireball.net/projects/markdown/syntax) for more details on the syntax, but the main thing to know is that `#` will be converted to `<h1>`, `##` to `<h2>` and so on. The asterisks denote an unordered list. The sentences in the middle will automatically be wrapped in `<p>` tags. We've added structure to the text, but kept things readable. 

Why bother? Why not go straight to HTML? Well, in this case the content is simple enough that sure, you might as well, but with much more complex, real-world content marking everything up in pure HTML is going to make it very difficult to read. Remember, this is the prototyping stage, things will be changing and you'll likely need to edit, rearrange and change your content many times. The more readable it is, the easier it is to make those structural changes.

### Tools for Converting to Plain Text

Unfortunately, it's rare that a client gives you plain text files. Most clients deliver content in MS Word files or PDFs or something even stranger. I had one client who sent over all their content as a series of PowerPoint presentations. 

The good news is that almost any files can be reduced to plain text. Regardless of how the client delivers the content, I convert it to plain text. For Word files I just open and save as plain text. That won't preserve any formatting, but you can keep the original around just to make sure you get the hierarchy and structure right. 

For PDFs I use a tool called [pdftotext](http://www.foolabs.com/xpdf/download.html). OS X users can grab a handy installer from [Carsten Blüm](http://www.bluem.net/en/mac/packages/). There are also numerous free online PDF-to-text converters, as well as OCR software available. If the client hands you content in PowerPoint slides you can open it in PowerPoint, save it as a Rich Text document and then open the Rich Text document in TextEdit or similar and save as plain text.

The point is to get your content in plain text form. 

### Pandoc for Fame and Fortune

The next step is to convert our Markdown-formatted file into an HTML file we can view in the browser using the server we set up above. There's a nearly unlimited number of ways we can convert from Markdown to actual HTML, but my favorite is [Pandoc](http://johnmacfarlane.net/pandoc/).

Installing Pandoc is simple, just head over to the [Pandoc download page](http://code.google.com/p/pandoc/downloads/list) and grab the installer for your platform (for OS X grab the .dmg file, for Windows grab the .msi). Then double click the installer and follow the directions. 

Once you have Pandoc installed you just need to run it on your markdown files. To do that fire up a terminal application. On OS X that would be Applications >> Utilities >> Terminal. On Windows you'll need [Cygwin](http://x.cygwin.com).

I know, the command line is antiquated, mysterious and a bit frightening for many people. 

I know that because it was that way for me too. But I kept noticing how much faster I could do things compared to visual apps. And I found that every time I used the terminal, it got a little less intimidating. I learned how to do one little thing that sped up my overall workflow. Then I learned another. And another. Today I use the terminal more than any other application. You don't have to go that far, but don't let it intimidate you. Just take it slow. Start with one thing that simplifies your life, like [the web server trick](https://longhandpixels.net/blog/2013/11/easiest-way-get-started-designing-browser). 

With that one already under your belt you're ready for Pandoc.

Open your terminal and navigate to your project folder. To stick with the previous tutorial in this series we'll say our project files are in `~/Sites/myproject`:

~~~{.language-bash}
cd ~/Sites/myproject
~~~

Now that we're in the right directory we just need to invoke Pandoc:

~~~{.language-bash}
pandoc -s --smart -t html5 -o about.html about.txt
~~~

This line says, take the file `about.txt`, convert it from Markdown to HTML5 (that's the `-t html5` bit) and save the results in a new file named `about.html`. The `-s` flag at the beginning of the line tells Pandoc that we want this to be a standalone conversion, which means Pandoc will add `<html>`, `<head>`, `<body>` and a few other tags so that we have an actual valid HTML file rather than just a fragment of HTML. Pandoc even adds a link to the [HTML5shiv](https://code.google.com/p/html5shiv/) for IE. 

The `--smart` flag turns on one little extra feature that converts straight quotes to actual or curly quotes. 

Point your web browser to <http://localhost:8080/about.html> and you should see the results. View source and you'll notice that Pandoc has done a bunch of stuff:

~~~language-markup
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="generator" content="pandoc">
    <title></title>
    <style type="text/css">code{white-space: pre;}</style>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <h1 id="were-awesome-widget-makers">We're Awesome Widget Makers</h1>
    <h2 id="crank-your-widgets-faster-with-acme-widgets">Crank your widgets faster with ACME Widgets</h2>
    <p>Our Widgets are the best in the business. You can rely on ACME widgets day in and day out. The toughest, most dependable widgets out there.</p>
    <h3 id="what-can-acme-widgets-do-for-you">What can ACME Widgets do for you?</h3>
    <ul>
        <li>We can help you build better subwidgets</li>
        <li>We can make your life widgetier</li>
        <li>We can solve all your widget headaches</li>
    </ul>
</body>
</html>
~~~
Pandoc has converted all our Markdown formatting into actual HTML. Our hashes change to header tags and our list gets marked up as an unordered list. Don't worry too much about the IDs and all the header elements. That can be changed or deleted when you move to working with templates in an actual content management system. 

As you can see we have a nicely structured HTML file which can serve as the basis of our templates or undergo further editing to add things like a site-wide navigation menu, header, logo and the like. 

Hmm. Maybe that list at the end should be an ordered list (probably not, but for example's sake, go with me here). Well, that's easy to change. Just open up the `about.txt` files and change the markdown to look like this:

~~~language-markup
#We're Awesome Widget Makers

##Crank your widgets faster with ACME Widgets

Our Widgets are the best in the business. You can rely on ACME widgets day in and day out. The toughest, most dependable widgets out there.

###What can ACME Widgets do for you?

1. We can help you build better subwidgets
2. We can make you life widgetier
3. We can solve all your widget headaches
~~~

Run the same Pandoc command (**tip**: if you hit the up arrow in your terminal app it will bring up the last used command. Assuming you've done nothing else in the mean time, that will be the Pandoc command we ran before). 

Refresh your browser and you should see that the list of things ACME widgets can do for you is now an ordered list. Hmm, maybe that header should be an H2? Maybe the client just called, they're sending over some updates for the page. None of that's a problem any more, you just update your text file, run Pandoc and see the results. Simple.

## Further

So far we've established a very basic, but fast workflow. We take our client provided content, convert it to text and with just two lines of code create HTML files and serve them locally for prototyping and structuring.

All this does is give you quick and dirty HTML you can use for prototyping. Why is that useful?

As Stephen Hay has [said repeatedly](http://www.the-haystack.com/) (and [written a book about](http://www.responsivedesignworkflow.com/), which you should read), starting with raw, unstyled HTML forces you to focus and prioritize. Hay suggests asking yourself, "what is the message that needs to be communicated if I was only able to provide them with unstyled HTML?" Start there, with the content -- the most important content -- and design everything around that.

We've got that basic unstyled HTML. What if you want to get a little bit fancier with Pandoc? Well, you certainly can. I do. 

In the next installment in this series we'll look at some advanced ways to use Pandoc including customizing the HTML template it uses, adding site-wide elements like navigation, headers and footers, as well as the part most designers all waiting for -- adding an actual stylesheet. 

So stay tuned. In the mean time, you can head over the [Pandoc documentation](http://johnmacfarlane.net/pandoc/README.html) if you'd like to get a head start.

[^1]: If you prefer a graphical download check out [Sitesucker](http://www.sitesucker.us/mac/mac.html) for OS X or [HTTrack](http://www.httrack.com) for Windows

If you want to learn some more handy tips and tricks for improving your responsive design workflows, I'm writing a book to teach you exactly that (and a whole lot more). Sign up for the mailing list below to hear more about the book and get a discount when it's released.

