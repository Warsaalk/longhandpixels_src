---
title: HTML5 Placeholder as a Label in Search Forms
pub_date: 2014-02-07 14:38:20
slug: /blog/2014/02/html5-placeholder-label-search-forms
metadesc: Using HTML5's placeholder attribute instead of a label is never a good idea. Except when maybe it is.
tags: Best Practices
code: True
tutorial: True
---

The HTML5 form input attribute `placeholder` is a tempting replacement for the good old `<label>` form element. 

In fact the web is littered with sites that use `placeholder` instead of labels (or worse, JavaScript to make the `value` attribute act like `label`).

Just because a practice is widespread does not make it a *best* practice though. Remember "skip intro"? I rest my case. Similarly, **you should most definitely not use `placeholder` as a substitute for form labels**. It may be a pattern on today's web, but it's a shitty pattern.

Labels help users complete forms. There are [mountains](http://rosenfeldmedia.com/books/web-form-design/) of [data](http://css-tricks.com/label-placement-on-forms/) and [eye tracking studies](http://www.uxmatters.com/mt/archives/2006/07/label-placement-in-forms.php) to back this up. If you want people to actually fill out your forms (as opposed, I guess, to your forms just looking "so clean, so elegant") then you want to use labels. The best forms, from a usability standpoint, are forms with non-bold, left aligned labels above the field they label.

Again, **using placeholder as a substitute for labels is a horrible UI pattern that you should (almost) never use.**

Is that dogmatic enough for you? Oh wait, *almost* never. Yes, I think there is one specific case where maybe this pattern makes sense: search forms.

Search forms are so ubiquitous and so well understood at this point that it may be redundant to have a label that says "search", a placeholder that also says "search" and a button that says "search" as well. I think just two of those would be fine. 

We could skip the placeholder text, which should really be more of a hint anyway -- e.g. "Jane Doe" rather than "Your Name" -- but what if we want to dispense with the label to save a bit of screen real estate, which can be at a premium on smaller viewports?

The label should still be part of the actual HTML, whether your average sighted user actually sees it or not. We need it there for accessibility. But with search forms, well, maybe you can tuck that label away, out of site. 

Progressive enhancement dictates that the labels should most definitely be there though. Let's consider a simple search form example:

~~~.language-markup
<form action="/search" method="get">
    <label id="search-label" for="search">Search:</label>
    <input type="text" name="search" id="query" value="" placeholder="Search LongHandPixels"> 
    <input class="btn" type="submit" value="Search">
</form>
~~~

Here we have our `<label>` tag and use the `for` attribute to bind it with the text input that is our search field. So far, so good for best practices. 

Here's what I think is the progressive enhancement route for search forms: use the HTML above and then use JavaScript and CSS to hide away the label when the it makes sense to do so. In other words, don't just hide the label in CSS. 

Hiding the label with something like `label {visibility: hidden;}` is a bad idea. That, and its evil cousin `display: none;` hide elements from screen readers and other assistive devices. Instead we'd want to do something like this:

~~~.language-css
.search-form-label-class {
    position: absolute; 
    left: -999em;
}
~~~

Check out Aaron Gustafson's ALA article <cite>[Now You See Me](http://alistapart.com/article/now-you-see-me)</cite> for more details on the various ways to hide things visually without hiding them from people who may need them the most. 

So this code is better, our label is off-screen and the placeholder text combined with descriptive button text serve the same purpose and still make the function of the form clear. The main problem we have right now is we've hidden the label in every browser, even browsers that won't display the `placeholder` attribute. That's not so great.

In this case you might argue that the button still makes the form function relatively clear, but I think we can do better. Instead of adding a rule to our stylesheet, let's use a bit of JavaScript to apply our CSS only if the browser understands the `placeholder` attribute. Here's a bit of code to do that:

~~~.language-javascript
<script>
if (("placeholder" in document.createElement("input"))) {
    document.getElementById("search-label").style.position= 'absolute';
    document.getElementById("search-label").style.left= '-999em';
}
 </script>
~~~

This is just plain JavaScript, if your site already has `jQuery` or some other library running them by all means use it's functions to select your elements and apply CSS. The point is the `if` statement,  which tests to see if the current browser support the `placeholder` attribute. If it does them we hide the label off-screen, if it doesn't then nothing happens. Either way the element remains accessible to screen readers.

If you'd like to see it in action, here's a working demo: [HTML5 placeholder as a label in search form](https://longhandpixels.net/demos/html5-placeholder/)

So is this a good idea? Honestly, I don't know. It might be splitting hairs. I think it's okay for search forms or other single field forms where there's less chance users will be confused when the placeholder text disappears. 

Pros:

* Saves space (no label, which can be a big help on small viewports)
* Still offers good accessibility

Cons:

* **Technically this is wrong**. I'm essentially using JavaScript to make `placeholder` take the place of a label, which is not what `placeholder` is for.
* **Placeholders can be confusing**. Some people won't start typing a search term because they're waiting for the field to clear. Others will think that the field is filled and the form can be submitted. See Chris Coyier's CSS-Tricks site for some ideas on how [you can make it apparent that the field](http://css-tricks.com/hang-on-placeholders/) is ready for input.
* **Not good for longer forms**. Again, multi-field forms need labels. Placeholders disappear when you start typing. If you forget which field you're in after you start typing, placeholders are no help. Despite the fact that the web is littered with forms that do this, please don't. Use labels on longer forms.

I'm doing this with the search form on this site. I started to do the same with my new mailing list sign up form (which isn't live yet), which is what got me writing this, thinking aloud as it were. My newsletter sign up form will have two fields, email and name (only email is required), and after thinking about this some more I deleted the JavaScript and left the labels.

If I shorten the form to just email, which I may, depending on some A/B testing I'm doing, then I may use this technique there too (and A/B test again). As of now though I don't think it's a good idea for that form for the reasons mentioned above.

I'm curious to hear from users, what do you think of this pattern? Is this okay? Unnecessary? Bad idea? Let me know what you think.

## Further Reading:

* The W3C's Web Platform Docs on the [placeholder](http://docs.webplatform.org/wiki/html/attributes/placeholder) attribute.
* Luke Wroblewski's book <cite>[Web Form Design](http://rosenfeldmedia.com/books/web-form-design/)</cite> is the Bible of building good forms.
* A little taste of Wroblewski's book over on his blog: [Web Application Form Design](http://www.lukew.com/ff/entry.asp?1502).
* UXMatter's once did some [eyeball tracking studies](http://www.uxmatters.com/mt/archives/2006/07/label-placement-in-forms.php) based on Wroblewski's book.
* Aaron Gustafson's ALA article [Now You See Me](http://alistapart.com/article/now-you-see-me), which talks about best practices for hiding elements with JavaScript.
* CSS-Tricks: [Hang On Placeholders](http://css-tricks.com/hang-on-placeholders/).
* [Jeremy Keith on `placeholder`](http://adactio.com/journal/6147/).
* CSS-Tricks: [Places It's Tempting To Use Display: None; But Don't](http://css-tricks.com/places-its-tempting-to-use-display-none-but-dont/). Seriously, don't.
{^ .list--indented }


