---
title: Responsive Images in Practice
mailing_list_title: Build a Better Web Vol 18
pub_date: 2014-11-11
slug: /newsletter/build-a-better-web-18
metadesc: Responsive images in practice, good workflow tips and the surprisingly high adoption rate of RWD. Oh and Mozilla releases a developer centric browser that's actually just a rebranded version of Firefox. 
template: newsletter 

---

Here's your weekly dose of noteworthy links on Responsive Design and related topics:

--

## Responsive Web Design

[Responsive Images in Practice](http://alistapart.com/article/responsive-images-in-practice) -- This is probably the clearest, most pragmatic guide to responsive images that I didn't write. Kidding. But not really, this is good so if you're still having trouble wrapping your head around it all -- and I don't blame you -- check this one out.

--

[Keeping srcset and sizes Under Control](https://mattwilcox.net/archives/keeping-srcset-and-sizes-under-control/) -- So using the first link you know what you want to do with responsive images, but what does the workflow actually look like? Developer Matt Wilcox has a great post on how to use the srcset and sizes attributes without losing your marbles. There can be a lot to keep track of if you're using a bunch of images, Wilcox has some good workflow tips.

--

[Responsive Web Design Adoption, 2014](http://www.guypo.com/rwd-2014/) -- Interesting collection of stats on responsive sites vs desktop-only sites. Of the top 10,000 sites on the web, nearly 20 percent of them are now responsive. That's considerably higher than I would have guessed. Of course most of them could do with some performance improvements, but we'll get there.

--

## Tools

[Firefox Developer Edition](https://developer.mozilla.org/en-US/Firefox/Developer_Edition) -- Sigh. I try to be optimistic about Mozilla's future, but then they go and do stuff like this. There's nothing here that's new beyond the theme. Which is not to say there are not some great tools in this browser, there are, but they're also all in Firefox Nightly and Aurora, which makes the whole Developer Edition thing look like a cheap PR move. Because, uh, it's a cheap PR move. 

--

## Site of the Week

[Free My Data](http://freemydata.co/) -- Nice site that takes a bunch of disparate info -- how to export your data from various web services -- and collects it all under one roof. Now go backup your stuff.
