---
title: Smaller Photos and Responsive Web Design Bloat
mailing_list_title: Build a Better Web Vol 3
pub_date: 2014-07-29
slug: /newsletter/build-a-better-web-3
metadesc: 'Mozilla improves JPEG. Sort of. Plus a great responsive images overview, simple sharing buttons, responsive web design bloat and more'
template: newsletter 

---

Hello lovely people of the web, welcome to the third edition of the Build a Better Web Newsletter.

Here's this week's roundup of responsive design links, along with html news, some cool websites and useful new tools.

##Responsive Web Design links:

[Improving JPEG Image Encoding](https://blog.mozilla.org/blog/2014/07/15/improving-jpeg-image-encoding/) -- Mozilla has never liked Google's WebP image format for some reason. Instead Mozilla set out to improve JPEG and did. Marginally at least. Personally I've been serving WebP to browsers that support it and have found that gets me around 30% smaller images on average (I just use the [PageSpeed module of Nginx](https://developers.google.com/speed/pagespeed/module/filter-image-optimize), which can automate the whole process).

--

[Responsive Images: Use Cases and Documented Code Snippets to Get You Started](http://dev.opera.com/articles/responsive-images/) -- The Opera Dev center on how to use the Picture element.

--

[Simple Sharing Buttons](http://simplesharingbuttons.com/) -- Developer Stefan Bohacek's Simple Sharing Buttons Generator helps you create social sharing buttons using only HTML and CSS. That means no JavaScript to slow down your pages. It also means your visitors won't be tracked by social services they may not even use. Good for performance and privacy. 

--

[RWD Bloat](http://daverupert.com/2014/07/rwd-bloat/) -- Developer Dave Rupert looks at performance and responsive design. Choice line: "If a page clocks in at 28MB and 399 HTTP Requests, that's not the fault of responsive design, that’s the fault an organization that doesn't care about web performance." Great practical look at RWD in the real world. Spoiler: RWD components add about 2% to the overall page weight.

--

[Ten CSS One-Liners to Replace Native Apps](http://alistapart.com/blog/post/ten-css-one-liners-to-replace-native-apps/) -- Håkon Wium Lie, co-creator of CSS, on CSS Figures. I'm honestly not sure what to think of this. I know Håkon has been working on various ways to [bring paged media](http://www.webmonkey.com/2011/10/css-paged-media-brings-book-smarts-to-the-web/) to the web for a very long time though, and like ALA says in the intro, "When Håkon speaks, whether we always agree or not, we listen".

--

[M dot or RWD. Which is faster?](http://bigqueri.es/t/m-dot-or-rwd-which-is-faster/296) -- This was supposed to be in issue one, about performance but I lost track of the tab. The answer here might surprise you. 


## HTML/CSS News:

[simpl.info](http://simpl.info/index.html) -- Nice, simple little reference for HTML, CSS, and JavaScript features. Each thing listed gets a simple (notice the theme here?) demo as well.

[GitHub's CSS](http://markdotto.com/2014/07/23/githubs-css/) -- Fascinating look at how GitHub handles CSS.

##Site of the week:

[AprilZero](http://aprilzero.com/) -- I think this may be what the future of personal websites looks like -- less blog, more log. 

--

Thanks for reading and if you have suggestions for next week, send them to scott@longhandpixels.net
